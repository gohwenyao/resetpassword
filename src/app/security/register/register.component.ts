import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.authService.setLogon(false);
  }

  goHome(){
    this.router.navigate(['']);
  }

  passReset: boolean = false;
  resetPassword() {
    this.authService.resetPassword(this.form-signup.value['email'])
        .then(() => this.passReset = true)
  }
}
