import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import * as firebase from 'firebase';

@Injectable()
export class AuthService {

  private isLoggedInSource = new Subject<boolean>();
  isLoggedIn$ = this.isLoggedInSource.asObservable();
  
  setLogon(logonFlag: boolean){
    this.isLoggedInSource.next(logonFlag);
  }

  logout(){
    
  }

  resetPassword(email: string) {
    var auth = firebase.auth();

    return auth.sendPasswordResetEmail(email)
        .then(() => console.log("email sent"))
        .catch((error) => console.log(error))
  }
  
}
